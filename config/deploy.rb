# config valid only for current version of Capistrano
# lock '3.10.0'

set :application, 'fgp-typo3'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default value for :scm is :git
set :scm,:copy
set :copy_source,   './'
set :copy_exclude, ['env*.yml', '.env.php','app/vendor/bundle/**', 'tmp/', 'Todo.md', 'backup', 'cache',\
 'Gemfile*', 'Capfile', 'Thumbs.db', 'composer.*', 'README.md', '^config', 'app/web/.htaccess', \
 'app/web/fileadmin', 'app/web/uploads', 'app/web/typo3temp', '.git', '.idea', '.gitignore', \
 '.DS_Store', '._.DS_Store', 'Documentation/', 'fixtures', 'docker-*', 'Dockerfile.*', \
 '^bin', '^docker',  '^etc' , '^frontend', '^konzeption', '^provision', '^test', 'bitbucket-pipelines.yml', 'Makefile' , 'Readme*'
 ]
set :default_file_access_mode_of_release_path, "660"

set :linked_dirs, %w{app/web/fileadmin app/web/uploads}
set :linked_files, %w{app/web/typo3conf/LocalConfiguration.php app/web/typo3conf/AdditionalConfiguration.php app/web/.htaccess}

# keep number of releases
set :keep_releases, 3
# temp directory on remote server
set :tmp_dir, '/tmp/deployment'

set :ssh_options, {
  user: 'web'
}

=begin
A list of tables which should be excluded from database export.
Note: Since we exclude be_users from export developers will have to
import the fixture/development/be_users.sql or create an admin and
command line user manually for their local setup
=end
set :db_export_exclude_tables, 'be_sessions,be_users,fe_sessions,fe_users,fe_session_data,cache_md5params,'\
    'cache_treelist,sys_log,be_users,fe_users,sys_domain,sys_history,sys_lockedrecords,'\
    'cf_cache_hash,cf_cache_hash_tags,cf_cache_imagesizes,cf_cache_imagesizes_tags,cf_cache_pages,cf_cache_pages_tags,'\
    'cf_cache_pagesection,cf_cache_pagesection_tags,cf_cache_rootline,cf_cache_rootline_tags,'\
    'cf_extbase_datamapfactory_datamap,cf_extbase_datamapfactory_datamap_tags,cf_extbase_object,'\
    'cf_extbase_object_tags,cf_extbase_reflection,cf_extbase_reflection_tags,cf_extbase_typo3dbbackend_queries,'\
    'cf_extbase_typo3dbbackend_queries_tags,tx_extensionmanager_domain_model_extension,tx_extensionmanager_domain_model_repository,'

set :php_bin_path, '/usr/local/bin/php7-71STABLE-CLI'

namespace :check do
  desc "Check that we can access everything"
  task :test_write_permissions do
    on roles(:all) do |host|
      if test("[ -w #{fetch(:deploy_to)} ]")
        info "#{fetch(:deploy_to)} is writable on #{host}"
      else
        error "#{fetch(:deploy_to)} is not writable on #{host}"
      end
    end
  end
  task :test_env_file_exists do
    on roles (:web) do |host|
      if test("[ -f #{fetch(:env_file_path)} ]")
        info "#{fetch(:env_file_path)} does exist on #{host}"
      else
        error "#{fetch(:env_file_path)} does not exist on #{host}"
      end
    end
  end
end

namespace :typo3 do
  desc "Install TYPO3"
  task :install do
    on roles(:web) do
      within release_path do
        execute "#{fetch(:php_bin_path)} #{release_path}/app/vendor/bin/typo3cms install:setup --non-interactive --site-name=#{fetch(:site_name)}"
      end
    end
  end

  desc "Clear TYPO3 cache"
  task :cache_flush do
    on roles(:web) do
      within release_path do
        execute "#{fetch(:php_bin_path)} #{release_path}/app/vendor/bin/typo3cms cache:flush"
      end
    end
  end

  desc "Update language"
  task :language_update do
    on roles(:web) do
      within release_path do
        execute "#{fetch(:php_bin_path)} #{release_path}/app/vendor/bin/typo3cms language:update"
      end
    end
  end

  desc "Dump database excluding a predefined list of tables"
  task :database_export do
    on roles(:db) do
        execute "#{fetch(:php_bin_path)} #{release_path}/app/vendor/bin/typo3cms database:export --exclude-tables #{fetch(:db_export_exclude_tables)} | gzip > #{fetch(:db_dump_file_path)}"
    end
  end

  desc "Download database dump"
  task :database_download_dump do
    on roles(:db) do |host|
        if test("[ -f #{fetch(:db_dump_file_path)} ]")
          info "Database dump found on #{host}."
          download! "#{fetch(:db_dump_file_path)}", "./backup/mysql.#{host}.sql.gz"
        else
          error "Database dump file #{fetch(:db_dump_file_path)} not found on #{host}"
        end
    end
  end

  desc "Update database schema"
  task :database_update do
    on roles(:db) do
      within release_path do
        execute "#{fetch(:php_bin_path)} #{release_path}/app/vendor/bin/typo3cms database:updateschema '*.add,*.change'"
      end
    end
  end
  desc "Fix TYPO3 folder structure (like in Install Tool)"
  task :fixfolderstructure do
    on roles(:web) do
      within release_path do
        execute "#{fetch(:php_bin_path)} #{release_path}/app/vendor/bin/typo3cms install:fixfolderstructure"
      end
    end
  end

  desc "Warm up cache"
  task :cache_warmup do
    on roles(:all) do
      within release_path do

      end
    end
  end

end


namespace :deploy do
  before :starting, "check:test_write_permissions"
  before :starting, "check:test_env_file_exists"
  #enable the following for actual deployment
  before :cleanup, "typo3:fixfolderstructure"
  #before :cleanup, "typo3:database_update"
  #before :cleanup, "typo3:cache_flush"
  #before :cleanup, "typo3:cache_warmup"
end
