freie-grundschule.de bundle
========================

Project bundle for the website freie-grundschule.de. 

### Overview
|     System       |     Domain                                  |
| ------------------------ | ----------------------------------- |
| **LIVE**         | http://freie-grundschule.de                |
| **STAGING**      | http://test.freie-grundschule.de   |
| **Git**          | https://gitlab.com/fgp/web/fgp-bundle |

### Requirements
* (local) web server
* mysql database for TYPO3
* composer
 
### Installation

#### Project Files
In your project directory execute:

```bash
git clone https://gitlab.com/fgp/web/fgp-bundle.git fgp
cd fgp
composer install
touch app/web/FIRST_INSTALL
```
This will create the necessary file structure for TYPO3 and allow the initial setup.

#### ddev
This project contains a configuration for a local development environment provided by
[ddev](http://ddev.readthedocs.io/).

Docker and _ddev_ must be installed. 

```bash
ddev start
```
_ddev_ creates all necessary containers. 

Now open the install page for your site in your browser: `http://fgp.ddev.local/install`

#### Credentials
Credentials for servers are provisioned by the server admins (via environment variables). The app relies on those credentials.
**Never** commit any credentials to any repo.

### Deployment
This project should only be deployed via gitlab pipeline. See `.gitlab-ci.yml`.
